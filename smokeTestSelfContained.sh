#!/bin/sh
echo "Starting test"
webserv=`kubectl describe service demo-frontend | grep Ingress | sed 's/^.*: //'   `
echo $webserv
portline=`kubectl describe service demo-frontend | grep Port: | sed 's/^.*: //' | sed -e 's/<unset>\(.*\)TCP/\1/'    `
echo $portline > t.txt
port=`cut -c-4 t.txt`
echo $port
full=$webserv":"$port
echo $full
keyword=$1
echo "curl" $full " | grep '" $keyword "'"
if curl $full | grep "$keyword" > /dev/null
then
    # if the keyword is in the content
    echo "success"
    exit 0
else
    echo "error"
    exit 1
fi
